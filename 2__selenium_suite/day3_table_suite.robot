*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    Log    xpath=//table[@id='example']/tbody/tr[1]/td[2]

TC2
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    FOR    ${i}    IN RANGE    1    11
        ${name}     Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
        Log    ${name}

    END

TC3
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    FOR    ${i}    IN RANGE    1    11
        ${name}     Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
        Log    ${name}
        #click on checkbox of Brenden Wagner
        #Approach1
        IF    '${name}' == 'Brenden Wagner'
         Click Element    xpath=//table[@id='example']/tbody/tr[${i}]/td[1]
         Exit For Loop
        END
        
        #Approach2
        Run Keyword If     '${name}' == 'Brenden Wagner'
        ...    Click Element    xpath=//table[@id='example']/tbody/tr[${i}]/td[1]
        Exit For Loop If    '${name}' == 'Brenden Wagner'

        #Approach3
        Run Keyword If    '${name}' == 'Brenden Wagner'
        ...    Run keywords    Click Element    xpath=//table[@id='example']/tbody/tr[${i}]/td[1]
        ...  AND    Log    ${name}

    END

TC4
    # string contains
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://datatables.net/extensions/select/examples/initialisation/checkbox.html
    FOR    ${i}    IN RANGE    1    11
        ${name}     Get Text    xpath=//table[@id='example']/tbody/tr[${i}]/td[2]
        Log    ${name}
        #click on checkbox of Brenden Wagner
        #Approach1
        IF     'Bradley' in  '${name}'
            Click Element    xpath=//table[@id='example']/tbody/tr[${i}]/td[1]
            Exit For Loop
        END
    END