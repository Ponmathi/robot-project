*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.db4free.net/
    Click Element    locator=xpath=//a[@href='/phpMyAdmin']
    #Click Element    locator=xpath=//a[contains(@href,'phpMyAdmin')] -- need to check
    #Click Element    partial link=phpMyAdmin
    
    Switch Window   phpMyAdmin
    Input Text    id=input_username    admin 
    Input Password    id=input_password    welcome123
    Click Element    id=input_go
    #Element Text Should Be    xpatj=//div[@role='alert']    expected
    Element Should Contain    id=pma_errors    Access denied for user
    Page Should Contain    Access denied for user
    Close Window
    Switch Window      db4free.net - MySQL Database for free

TC2 Popup_check
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.online.citibank.co.in/
    # close if any popup comes
    #popsec
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose']
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose2']
        #click on Login
    Click Element    xpath=//span[text()='Login']
    #Click on Forgot User ID?
    Switch Window   Citibank India
    Click Element    xpath=//div[contains(text(),' Forgot User')]
    #Choose Credit Card
    Click Element    link=select your product type
    Click Element    link=Credit Card
    #Enter credit card number as 4545 5656 8887 9998
    Input Text    name=citiCard1    4545
    Input Text    name=citiCard2    5656
    Input Text    name=citiCard3    8887
    Input Text    name=citiCard4    9998

    #Enter cvv number
    Input Text    name=CCVNO    234
    #Enter date as “14/04/2022”
    #Approach 1 -  if the date field is not readonly
    #Input Text    id=bill-date-long    14/12/2022
    #Approach 2
    #Click Element    xpath=//input[@id='bill-date-long']
    #Select From List By Label    xpath=//select[@data-handler='selectYear']     2022
    #Select From List By Label    xpath=//select[@data-handler='selectMonth']     Dec
    #Click Element    link=14
    #Approach 3 - using Javascript
    Execute Javascript  document.querySelector('#bill-date-long').value='14/12/2022'

    #Click on Proceed
    Click Element    css=[value='PROCEED']
    #Get the text and print it “Please accept Terms and Conditions”
    Element Should Contain    xpath=//li[contains(text(),'accept Terms')]
    ...     Please accept Terms and Conditions


TC3 Popup Count
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.online.citibank.co.in/
    ${ele_count}    Get Element Count    xpath=//a[@class='newclose']
    #click when count > 0
    IF    ${ele_count}>0
         Click Element    xpath=//a[@class='newclose']
    END
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose2']
    Run Keyword And Ignore Error    Click Element    xpath=//a[@class='newclose3']