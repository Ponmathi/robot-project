*** Settings ***
Library    SeleniumLibrary

*** Test Cases ***
TC1
    Open Browser    url=https://github.com/login   browser=chrome
    Maximize Browser Window
    Input Text    id=login_field    hello
    Input Password      id=password     89hello
    Click Element    name=commit
    #validate the error message
    Element Text Should Be    xpath=//div[@role='alert']    Incorrect username or password.

TC2
    Open Browser    https://www.salesforce.com/in/form/signup/freetrial-sales/      browser=chrome
    Maximize Browser Window
    Input Text    name=UserFirstName    John
    Input Text    name=UserLastName    wick
    Input Text    name=UserEmail    john@gmail.com
    Select From List By Value   name=UserTitle     IT_Manager_AP
    Select From List By Label    name=CompanyEmployees      201 - 500 employees
    Select From List By Label    name=CompanyCountry       United Kingdom
    Click Element    locator=xpath=//div[@class='checkbox-ui']
    Click Element    locator=xpath=//button[@name='start my free trial']
    Element Text Should Be    xpath=//span[contains(@id,'UserPhone')]   Enter a valid phone number

TC3
    Open Browser    https://www.medibuddy.in/      browser=chrome
    Maximize Browser Window