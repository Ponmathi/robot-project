*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1
    #upload the file
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://www.ilovepdf.com/pdf_to_word
    Choose File    xpath=//input[@type='file']     c:${/}Automation Concepts${/}text.pdf

TC2
    #Mouse
      Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://nasscom.in/about-us/contact-us
    Mouse Over    xpath=//a[text()='Membership']
    #mouse over on Become a member
    Mouse Over    xpath=//a[text()='Become a Member']
    Click Element    xpath=//a[text()='Membership Benefits']

TC4 Php Javascript
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://phptravels.net
    #checkin 19-08-2023
    Execute Javascript  document.querySelector('#checkin').value='19-08-2023'
    #checkout 26-08-2023
    #Execute Javascript  document.querySelector('#checkout').value='26-08-2023'

    Execute Javascript  document.querySelector("input[name='checkout']").value='26-08-2023'

    #city Mysore,India
    #adult 4, child 2

    #nationality - canada

TC5 Php Javascript without knowing javascript
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://phptravels.net
    #checkin 19-08-2023
    ${elem}     Get WebElement    xpath=//input[id='checkin']
    Execute Javascript  argument[0]='19-08-2023'    ARGUMENTS   ${elem}
    #checkout 26-08-2023
    ${checkout_elem}     Get WebElement    xpath=//input[id='checkout']
    Execute Javascript  argument[0]='26-08-2023'    ARGUMENTS   ${checkout_elem}