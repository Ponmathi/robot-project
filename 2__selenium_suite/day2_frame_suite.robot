*** Settings ***
Library     SeleniumLibrary


*** Test Cases ***
TC1
    Open Browser    browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=https://netbanking.hdfcbank.com/netbanking
    #enter userid as test123 throws error - becoz it is inside the frame
    Select Frame    xpath=//frame[@name='login_page']
    Input Text    xpath=//input[@name='fldLoginUserId']    test123
    #click on continue
    #Click Element    xpath=//a[text()='CONTINUE']
    Click Element    link='CONTINUE'
    
    Unselect Frame