*** Settings ***
Library     SeleniumLibrary

*** Test Cases ***
TC1 FB Login
    Open Browser    url=https://www.facebook.com/   browser=chrome
    #get title and print it
    ${title}    Get Title
    Log To Console    ${title}
    
TC2
    Open Browser    url=https://www.facebook.com/   browser=chrome
    Input Text    id=email    test@gmail.com
    #Input Password    id:pass    text111
    Input Password    locator=id=pass    password=text111
    Click Element    name=login
    Close Browser
    
TC3
    Open Browser    url=https://www.facebook.com/   browser=chrome
    Maximize Browser Window
    Set Selenium Implicit Wait    30s
    #click on create account
    Click Element    link=Create new account
    #enter first name
    #checks the presence of locator in 0.5s
    #Sleep    5s -- Not recommended
    Input Text    name=firstname    John
    #enter lastname as wick
    Input Text    name=lastname    wick
    #enter password as welcome123
    Input Password    name=reg_passwd__    welcome123
    #20 Dec 2020
    Select From List By Value    id=day     20
    Select From List By Label    id=month   Dec
    Select From List By Index    id=year    3
    #click on radio button Custom
    Select Radio Button    group_name=sex    value=-1
    #Click Element    css=label#_58mt
    #Click Element    xpath=//input[@value='-1']
    #Click Element    xpath={//input[@name='sex']}[3]
