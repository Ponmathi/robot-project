*** Settings ***
Library     SeleniumLibrary

*** Keywords ***
Launch Browser
    [Arguments]     ${browser_name}     ${url}
    Open Browser    browser=${browser_name}
    Maximize Browser Window
    Set Selenium Implicit Wait    20s
    Go To    url=${url}


*** Test Cases ***
TC1
    Launch Browser      chrome    http://demo.openemr.io/b/openemr/
    Input Text    css=#authUser    admin
    Input Password    css=#clearPass    pass
    Select From List By Label    css=select[name='languageChoice']  English (Indian)
    Click Element    css=#login-button
    #Mouse Over    locator
    Click Element    xpath=//div[text()='Patient']
    Click Element    xpath=//div[text()='New/Search']
    Select Frame    xpath=//iframe[@name='pat']
    Input Text    id=form_fname     test
    Input Text    id=form_lname    test
    Input Text    id=form_DOB    2023-05-24
    Select From List By Label    id=form_sex    Female

    Click Element    id=create
    Unselect Frame
    Select Frame    xpath=//iframe[@id='modalframe']
    #Select Frame    xpath=//iframe[contains(src,'new_')]
    Click Element    xpath=//button[normalize-space()='Confirm Create New Patient']
    Unselect Frame
    ${actual_alert_text}    Handle Alert    action=ACCEPT   timeout=20s
    Run Keyword And Ignore Error    Click Element    xpath=//div[@class='closeDlgIframe']

    Select Frame    xpath=//iframe[@name='pat']
    Element Should Contain    xpath=//span[contains(text(),'Record Dashboard')]    test test
    Element Text Should Be    xpath=//span[contains(text(),'Record Dashboard')]    Medical Record Dashboard - test test
    Unselect Frame
    Should Contain    ${actual_alert_text}    Tobacco
    Close Browser
