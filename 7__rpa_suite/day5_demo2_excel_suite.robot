*** Settings ***
Library     RPA.Excel.Files

*** Test Cases ***
TC1
    Open Workbook    path=${EXECDIR}${/}test_data${/}details.xlsx
    @{sheet_names}      List Worksheets
    Log    ${sheet_names}

    FOR    ${sheet_name}    IN    @{sheet_names}
        Log    ${sheet_name}
    END

TC2
    Open Workbook    path=${EXECDIR}${/}test_data${/}details.xlsx
    ${sheet}    Read Worksheet  test
    Log    ${sheet}
    Log    ${sheet}[0]
    #read the A th column
    Log    ${sheet}[0][A]
    #read the B th column
    Log    ${sheet}[0][B]
    
    ${row_count}      Get Length    ${sheet}
    Log    ${row_count}
    ${col_count}      Get Length    ${sheet}[0]
    Log    ${col_count}

TC3
    Open Workbook    path=${EXECDIR}${/}test_data${/}details.xlsx
    ${sheet}    Read Worksheet  test
    Set Worksheet Value    1    1    hello
    Save Workbook
    ${value}    Get Worksheet Value    1    1
    Log    ${value}