*** Settings ***
Library     RPA.Word.Application

*** Test Cases ***
TC1 New Document
    Open Application
    Create New Document
    Write Text    text=In Robot Training
    Save Document As    filename=c:${/}Automation Concepts${/}data.docx
    Quit Application
    
TC2 Open Existing Document
    Open Application
    Open File    filename=c:${/}Automation Concepts${/}data.docx
    ${texts}    Get All Texts
    Log    ${texts}