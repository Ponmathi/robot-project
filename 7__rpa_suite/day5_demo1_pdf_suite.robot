*** Settings ***
Library     RPA.PDF
Library    Collections

*** Test Cases ***
TC1
    ${res}      Get Text From Pdf   source_path=${EXECDIR}${/}test_data${/}PYTHON PROGRAMMING NOTES.pdf
    Log    ${res}

    ${page}     Convert To Integer    1
    Log    ${res}[${page}]

    ${keys}     Get Dictionary Keys     ${res}
    Log    ${keys}

    FOR    ${value}    IN    @{keys}
        Log    ${value}[${keys}]

    END