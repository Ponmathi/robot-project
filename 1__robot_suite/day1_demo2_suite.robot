*** Settings ***
Library    String


*** Test Cases ***
TC1
    ${name}     Set Variable    robot session
    #convert to uppercase and print
    ${name}    Convert To Upper Case   ${name}
    Log To Console    ${name}

TC2
    ${num1}     Set Variable    $102,000
    ${num2}     Set Variable    $201,500,000
    ${num1}     Remove String    ${num1}    $      ,
    ${num2}     Remove String    ${num2}    $      ,
    ${num1}     Convert To Number    ${num1}
    #sum above two values and print 
    ${result}   Evaluate    ${num1}+${num2}
    Log To Console    ${result}
    Log    ${result}