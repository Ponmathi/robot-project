*** Settings ***
Library    Collections

*** Variables ***
${BROWSER_NAME}     chrome
@{COLORS}       red     yellow      pink
&{MY_DETAILS}   name=Ponmathi   role=Developer  node=BEAM

*** Test Cases ***
TC1
    Log To Console    ${BROWSER_NAME}

TC2
    Log To Console    ${BROWSER_NAME}
    Log To Console    ${COLORS}
    Log To Console    ${COLORS}[1]
    ${size}     Get Length    ${COLORS}
    Log To Console    ${size}

TC3
    @{fruits}   Create List     mango   orange    grapes
    #print the size of the list
    ${length}   Get Length    ${fruits}
    Log To Console    ${length}
    Log    ${fruits}
    #append apple to the list
    Append To List  ${fruits}       apple
    Log To Console    ${fruits}
    Log    ${fruits}
    #remove grapes from the list
    #Remove From List    ${fruits}    2
    Remove Values From List    ${fruits}    grapes
    Log To Console    ${fruits}
    Log    ${fruits}
    #insert jackfruit at index 0
    Insert Into List    ${fruits}    0    jackfruit
    #print the list
    Log To Console    ${fruits}
    Log    ${fruits}
    
TC4
    Log To Console    ${MY_DETAILS}
    Log To Console    ${MY_DETAILS}[role]
    Log To Console    ${MY_DETAILS}[node]
    
TC5
    &{dic}  Create Dictionary   header=application/json     body=test
    Log To Console    ${dic}
    