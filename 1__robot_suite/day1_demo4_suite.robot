*** Settings ***
Library     OperatingSystem
Library    Collections

*** Test Cases ***
TC1
    #Create Directory    path
    Log To Console    c:\Automation Concepts\hellofolder
    Log To Console    c:\\Automation Concepts\\hellofolder
    Log To Console    c:${/}Automation Concepts${/}hellofolder
    
TC2
    Create Directory    path=c:${/}Automation Concepts${/}hellofolder   
    Directory Should Exist    path=c:${/}Automation Concepts${/}hellofolder
    #Remove Directory    path=c:${/}Automation Concepts${/}hellofolder
    #Directory Should Not Exist    path=c:${/}Automation Concepts${/}hellofolder
    
TC3
    #list all files in the directory c:\Program Files\WinRAR
    ${count}    Count Files In Directory    path=c:${/}Program Files${/}WinRAR
    Log To Console    ${count}
    @{files}    List Files In Directory    path=c:${/}Program Files${/}WinRAR
    Log List    ${files}
    Log    ${files}[1]

TC4
    Get Environment Variable    path
    
TC5
    Log To Console    ${CURDIR}    
    Log To Console    ${OUTPUT_DIR}
    Log To Console    ${EXECDIR}
    Log To Console    ${LOG_LEVEL}
    Log To Console    Environment${SPACE}${SPACE}variable
    Log To Console    ${TEMPDIR}
    Log To Console    ${SUITE_NAME}
    Log To Console    ${TEST_NAME}