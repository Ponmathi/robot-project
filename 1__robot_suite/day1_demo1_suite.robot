*** Settings ***
Library     DateTime

*** Test Cases ***
TC1
    Log To Console    Ponmathi
    
TC2
    Log To Console    message=Welcome to Day1 Robot Training   
    Log To Console    Ponmathi
    
TC3
    ${my_name}  Set Variable    Ponmathi
    Log To Console    ${my_name}     
    Log    ${my_name}

TC4
    ${radius}   Set Variable    10
    #Write logic to calculate area of circle
    ${result}   Evaluate    3.14*${radius}*${radius}
    Log To Console    ${result}

TC5
    ${current_date}     Get Current Date
    Log To Console    ${current_date}
    Log    ${current_date}