*** Settings ***
Library     SSHLibrary


*** Test Cases ***
TC1
    Open Connection     host=<ipaddress>   port=22
    Login   username=   password=
    ${stdout}=  Execute Command     git --version
    Log To Console    ${stdout}

TC2
    Open Connection     alias=test  host=1.1.1.1   port=22
    Login   username=   password=
    ${stdout}=  Execute Command     python --version
    Log To Console    ${stdout}

TC3
    Open Connection     alias=test  host=1.1.1.1   port=22
    Open Connection     alias=test1  host=11.11.11.11   port=22

    Switch Connection    index_or_alias=test
    Login   username=ee   password=sdsf
    Get File    source=C:${/}test${/}future.txt     destination=C:${/}demo${/}future.txt
    ${stdout}=  Execute Command     python --version
    Log To Console    ${stdout}
    
