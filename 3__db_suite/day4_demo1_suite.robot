*** Settings ***
Library     DatabaseLibrary

Suite Setup     Connect To Database     dbapiModuleName=pymysql     dbName=dbfree_db    dbPassword=12345678
       ...      dbUsername=dbfree_db    dbHost=db4free.net   dbPort=3306

Suite Teardown      Disconnect From Database

*** Comments ***
Table  - Products
Columns - product_id, productname, description

*** Test Cases ***
TC1
    ${row_count}    Row Count    select * from Products
    Log To Console    ${row_count}
    
TC2
    Row Count Is Equal To X    select * from Products    153    
    
TC3
    Row Count Is Greater Than X    select * from Products    100
    Row Count Is Less Than X    select * from Products    200
    Row Count Is 0    select * from Products where product_id=7883234
    
TC4
    ${description}   Description    select * from Products
    Log To Console    ${description}

TC5
    @{output}   DatabaseLibrary.Query    select * from Products
    Log    ${output}
    Log Many    ${output}
    Log     ${output}[0]
    Log     ${output}[0][1]
    #return the size of the list
    ${size}     Get Length    ${output}
    Log    ${size}

    FOR    ${i}    IN RANGE    0    ${size}
        Log    ${output}[${i}][1]
    END

TC6
    @{output}   DatabaseLibrary.Query    select * from Products where product_id=995678
    Log    ${output}
    Log    ${output}[0][0]
    Log    ${output}[0][1]
    Log    ${output}[0][2]
    
TC7 Delete
    DatabaseLibrary.Execute Sql Script    Delete from Products where product_id=995678
    Row Count Is 0    select * from Products where product_id=995678
    Check If Not Exists In Database    select * from Products where product_id=995678    
    
TC8 Insert
    Execute Sql String
    ...   Insert into Products (product_id, ProductName, description) values ('22', 'testing', 'test')
    Row Count Is Equal To X    select * from Products where product_id=22    1

TC9 Update
    Execute Sql String    Update Products set ProductName='test123' where product_id=22
    @{output}   DatabaseLibrary.Query    select * from Products where product_id=22
    Log    ${output}