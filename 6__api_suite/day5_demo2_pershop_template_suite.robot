*** Settings ***
Library     RequestsLibrary

*** Test Cases ***



*** Keywords ***
TC1 Find Valid Pet By Id
    [Arguments]     ${pet_id}
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    ${response}     GET On Session    alias=petshop     url=${pet_id}   expected_status=200
    Log    ${response}
    Log    ${response.json()}
    Log    ${response.status_code}
    Status Should Be    200
    Log    ${response.json()}[id]
    Should Be Equal As Integers    ${response.json()}[id]    5