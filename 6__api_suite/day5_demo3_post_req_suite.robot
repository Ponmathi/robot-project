*** Settings ***
Library     RequestsLibrary
Library    OperatingSystem

*** Test Cases ***
TC1 Add Valid Pet
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2

    &{header_dic}   Create Dictionary   Content-Type=application/json   Connection=keep-alive

    ${json}     Get Binary File     ${EXECDIR}${/}test_data${/}new_pet.json
    ${response}     POST On Session     alias=petshop   url=/pet    headers=${header_dic}   data=${json}    expected_status=200
    Should Be Equal As Integers    ${response.json()}[id]   6565
    Should Be Equal As Strings    ${response.json()}[name]    fighterfish
