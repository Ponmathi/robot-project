*** Settings ***
Library     RequestsLibrary
Library    OperatingSystem

*** Test Cases ***
TC1 Update Valid Pet
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2
    &{header_dic}   Create Dictionary   Content-Type=application/json   Connection=keep-alive

    ${json}     Get Binary File     ${EXECDIR}${/}test_data${/}update_pet.json
    ${response}     Put On Session    alias=petshop   headers=${header_dic}   url=/pet  data=${json}    expected_status=200
    Should Be Equal As Integers    ${response.json()}[id]   6565
    Should Be Equal As Strings    ${response.json()}[name]    fighter fish111