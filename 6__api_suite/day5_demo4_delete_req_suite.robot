*** Settings ***
Library     RequestsLibrary
Library    OperatingSystem

*** Test Cases ***
TC1 Delete Valid Pet By Id
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2

    &{header_dic}   Create Dictionary   api_key=sdf32sdg2212

    ${response}     DELETE On Session   alias=petshop   url=pet/6565   headers=${header_dic}
    
    Log    ${response}

TC2 Delete Invalid Pet By Id
    Create Session    alias=petshop    url=https://petstore.swagger.io/v2

    &{header_dic}   Create Dictionary   api_key=sdf32sdg2212

    ${response}     DELETE On Session   alias=petshop   url=pet/6565   headers=${header_dic}    expected_status=404

    Log    ${response}